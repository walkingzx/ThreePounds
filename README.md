# ThreePounds



相关资料:


hyperledger 初始化: https://hyperledgercn.github.io/hyperledgerDocs/build_network_zh/

Chaincode 运行: https://blog.csdn.net/pony_maggie/article/details/78498558

开发文档 中文: https://hyperledgercn.github.io/hyperledgerDocs/chaincode_developers_zh/#2

EncCC使用示例: https://blog.csdn.net/greedystar/article/details/80604393

EncCC使用Readme: https://github.com/yeasy/docker-compose-files/blob/master/hyperledger_fabric/v1.1.0/examples/chaincode/go/enccc_example/README.md

Vondor使用: https://github.com/kardianos/govendor


以下为三个终端的测试命令:

1:

docker-compose -f docker-compose-simple.yaml up

2:

docker exec -it chaincode bash
cd cvChain
go build
CORE_PEER_ADDRESS=peer:7052 CORE_CHAINCODE_ID_NAME=mycc:0 ./cvChain


3:
//初始化
docker exec -it cli bash
peer chaincode install -p chaincodedev/chaincode/cvChain -n mycc -v 0
peer chaincode instantiate -n mycc -v 0 -c '{"Args":["a","10"]}' -C myc

//插入
peer chaincode invoke -n mycc -c '{"Args":["addRecord","1001","1999","college1","bachelor"]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["addRecord","1009", "2002","institute1","master"]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["addRecord","1001", "2006","corp1", "engineer"]}' -C myc

//查询
peer chaincode invoke -n mycc -c '{"Args":["addRecord","19940328", "2018","school", "master"]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["addRecord","19940328", "2016","school", "bachelor"]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["addRecord","19931112", "2018","corp1", "engineer"]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["addRecord","19931112", "2016","school", "master"]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["addRecord","19940328", "2016","school", "bachelor"]}' -C myc

peer chaincode query -n mycc -c '{"Args":["getRecord","1001", "1999"]}' -C myc
peer chaincode query -n mycc -c '{"Args":["getRecord","19940328", "2018"]}' -C myc

//生成KEY和IV
ENCKEY=`openssl rand 32 -base64` && DECKEY=$ENCKEY
IV=`openssl rand 16 -base64`


//加密插入
peer chaincode invoke -n mycc -C myc -c '{"Args":["encRecord","1009","2002","college2","bachelor"]}' --transient "{\"ENCKEY\":\"$ENCKEY\",\"IV\":\"$IV\"}"

//解密读取
peer chaincode query -n mycc -C myc -c '{"Args":["decRecord", "1009", "2002"]}' --transient "{\"DECKEY\":\"$DECKEY\"}"


