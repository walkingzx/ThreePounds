package main

import (
    "encoding/json"
    "fmt"
    "errors"
    
    "github.com/hyperledger/fabric/bccsp"
    "github.com/hyperledger/fabric/bccsp/factory"
    "github.com/hyperledger/fabric/core/chaincode/shim"
    "github.com/hyperledger/fabric/core/chaincode/shim/ext/entities"

    pb "github.com/hyperledger/fabric/protos/peer"
)

const ENCKEY = "ENCKEY"
const DECKEY = "DECKEY"
const IV = "IV"

type CvChain struct {
    bccspInst bccsp.BCCSP
}

type Record struct{
    Condition   string `json:"condition"`
    Position   string `json:"position"`
}

func (t *CvChain) Init(stub shim.ChaincodeStubInterface) pb.Response {
    return shim.Success(nil)
}

func (t *CvChain) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
    function, args := stub.GetFunctionAndParameters()
    var result string
    var err error
    if function == "addRecord" {
        result, err = addRecord(stub, args)
    } else if function == "getRecord" {
        result, err = getRecord(stub, args)
    } else if function ==  "encRecord" {
        tMap, err := stub.GetTransient()
        if err != nil {
            return shim.Error(fmt.Sprintf("Could not retrieve transient, err %s", err))
        }
        return t.Enc(stub, args[0:], tMap[ENCKEY], tMap[IV])
    }else if function == "decRecord" {
        tMap, err := stub.GetTransient()
        if err != nil {
            return shim.Error(fmt.Sprintf("Could not retrieve transient, err %s", err))
        }
        return t.Dec(stub, args[0:], tMap[DECKEY])
    }

    if err != nil {
        return shim.Error(err.Error())
    }
    return shim.Success([]byte(result))
}


//保存简历信息
func addRecord(stub shim.ChaincodeStubInterface, args []string) (string, error) {
    var key = args[0] + "_" + args[1]
    var value = Record {Condition: args[2], Position: args[3]}
    recordAsBytes, _ := json.Marshal(value)
    err := stub.PutState(key, recordAsBytes)
    if err != nil {
        return "", fmt.Errorf("Failed to set record: %s", key)
    }
    return string(recordAsBytes), nil
}

// 查询简历信息
func getRecord(stub shim.ChaincodeStubInterface, args []string) (string, error) {
    var key = args[0] + "_" + args[1]
    value, err := stub.GetState(key)
    if err != nil {
        return "", fmt.Errorf("Failed to get record: %s with error: %s", key, err)
    }
    if value == nil {
        return "", fmt.Errorf(fmt.Sprintf("Record not found: %s", key))
    }
    record := Record{}
    json.Unmarshal(value, &record)
    return record.Condition, nil
}

//加密
func (t *CvChain) Enc(stub shim.ChaincodeStubInterface, args []string, encKey, IV []byte) pb.Response {
    // generate ent
    ent, err := entities.NewAES256EncrypterEntity("ID", t.bccspInst, encKey, IV)
    if err != nil {
        return shim.Error(fmt.Sprintf("Fail to use NewAES256EncrypterEntity, err %s", err))
    }
    //set data
    var key = args[0] + "_" + args[1]
    var value = Record{Condition: args[2], Position: args[3]}
    recordAsBytes, _ := json.Marshal(value)

    // encryt the value and assign it to the key
    err = encryptAndPutState(stub, ent, key, recordAsBytes)
    if err != nil {
        return shim.Error(fmt.Sprintf("Fail to use encryptAndPutState, err %s", err))
    }

    return shim.Success(nil)
}

//使用加密器实体对value进行加密
func encryptAndPutState(stub shim.ChaincodeStubInterface, ent entities.Encrypter, key string, value []byte) error {
    ciphertext, err := ent.Encrypt(value)
    if err != nil {
        return err
    }
    return stub.PutState(key, ciphertext)
}

//解密
func (t *CvChain) Dec(stub shim.ChaincodeStubInterface, args []string, denKey []byte) pb.Response {
    ent, err := entities.NewAES256EncrypterEntity("ID", t.bccspInst, denKey, nil)
    if err != nil {
        return shim.Error(fmt.Sprintf("Fail to use NewAES256EncrypterEntity, err %s", err))
    }

    var key = args[0] + "_" + args[1]
    //解密实体器
    v, err := getStateAndDecrypt(stub, ent, key)
    if err != nil {
        return shim.Error(fmt.Sprintf("Fail to use getStateAndDecrypt, err %s", err))
    }
    record := Record{}
    json.Unmarshal(v, &record)

    return shim.Success([]byte(record.Condition))
}

func getStateAndDecrypt(stub shim.ChaincodeStubInterface, ent entities.Encrypter, key string) ([]byte, error) {
    ciphertext, err := stub.GetState(key)
    if err != nil {
        return nil, err
    }
    if len(ciphertext) == 0 {
        return nil, errors.New("no ciphertext to decrypt")
    }
    return ent.Decrypt(ciphertext)
}

func main() {
    //使用null工厂模式
    factory.InitFactories(nil)
    err := shim.Start(&CvChain{factory.GetDefault()})
    if err != nil {
        fmt.Printf("Error starting chaincode: %s", err)
    }
}